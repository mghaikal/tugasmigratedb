<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', 'MainController@master');

Route::get('/form', 'MainController@create');

Route::post('/casts', 'MainController@submit');

Route::get('/casts', 'MainController@viewcasts');

Route::get('/infocasts/{castsid}', 'MainController@infocasts');

Route::get('/infocasts/{castsid}/edit', 'MainController@editcasts');

Route::put('/infocasts/{castsid}', 'MainController@update');

Route::delete('/casts/{castsid}', 'MainController@del');
