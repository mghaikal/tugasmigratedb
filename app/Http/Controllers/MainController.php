<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class MainController extends Controller
{
    //
    public function master(){
        return view('layouts.master');
    }

    public function create(){
        return view('contents.create');
    }

    public function submit(Request $Request){
        //dd($Request->all());
        //return view('layouts.master');
        $Request->validate([
            "nama" =>'required',
            "umur" =>'required',
            "bio" => 'required'
        ]);
        $query = DB::table('casts')->insert([
            "nama" => $Request["nama"],
            "umur" => $Request["umur"],
            "bio" => $Request["bio"]
        ]);
        return redirect('/casts')->with('success', 'Data Berhasil Disimpan');
    }

    public function viewcasts(){
        $query  = DB::table('casts')->get();
        return view('contents.casts', compact('query'));
    }

    public function infocasts($castsid){
        $query = DB::table('casts')->where('id', $castsid)->first();
        return view('contents.infocasts', compact('query'));
    }

    public function editcasts($castsid){
        $query = DB::table('casts')->where('id', $castsid)->first();
        return view('contents.editcasts', compact('query'));      
    }

    public function update($id, Request $Request){
        $Request->validate([
            "nama" =>'required',
            "umur" =>'required',
            "bio" => 'required'
        ]);
        $query = DB::table('casts')->where('id', $id)
                -> update([
                    'nama' => $Request['nama'],
                    'umur' => $Request['umur'],
                    'bio' => $Request['bio']
                ]);
        return redirect('/casts')->with('success', 'Data Berhasil Diubah');

    }

    public function del($id){
        $query = DB::table('casts')->where('id', $id)->delete();
        return redirect('/casts')->with('success', 'Data Berhasil Dihapus');
    }

}
