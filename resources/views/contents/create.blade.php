@extends('layouts.master')


@section('content')
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Masukan data Aktor/Aktris</h3>
              </div>
              <!-- /.card-header -z->
              <!-- form start -->
              <form for ="form" action="/casts" method ="POST">
                  @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama Aktor/Aktris</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', '') }}" placeholder="Masukkan Nama">
                    @error('nama')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    </div>
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur', '') }}" placeholder="Masukkan Umur">
                    @error('umur')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="bio">Biografi</label>
                    <input type="text" class="form-control" id="bio" name="bio" value="{{ old('bio', '') }}" placeholder="Beritahu kami tentangmu!">
                    @error('bio')
                        <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
        </div>
      </div><!-- /.container-fluid -->
    </section>

@endsection