@extends('layouts.master')

@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Aktor/Aktris</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                    <div class = "alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 5%">No</th>
                      <th>Nama</th>
                      <th style="width: 5%">Umur</th>
                      <th style="width: 40%">Biografi</th>
                      <th style="width: 20%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @forelse($query as $key => $query)
                      <tr>
                        <td> {{$key + 1}} </td>
                        <td> {{$query -> nama}} </td>
                        <td> {{$query -> umur}} </td>
                        <td> {{$query -> bio}} </td>
                        <td style = "display: flex">
                            <a class = "btn btn-primary btn-sm m-1" href="/infocasts/{{$query->id}}">info</a>
                            <a class = "btn btn-success btn-sm m-1" href="/infocasts/{{$query->id}}/edit">edit</a>
                            <form  action="/casts/{{$query -> id}}" method ="post">
                            @csrf    
                            @method('DELETE')
                            <input type="submit" class = "btn btn-danger btn-sm m-1" value = "delete">
                        </form>
                        </td>
                        <tr>    
                     @empty 
                    </tr>
                          <td colspan="5" align="center">No Data</td>
                      </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body 
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
            -->
            </div>
@endsection